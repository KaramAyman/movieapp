package com.example.karam.newmovieapp;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karam on 8/26/2018.
 */

public class NewAdapter extends RecyclerView.Adapter<NewAdapter.ForecastAdapterViewHolder> {

    List<Result> mMovies;
    private final ForecastAdapterOnClickHandler mClickHandler;

    public void setMovieData(List<Result> movieData) {
        mMovies = movieData;
        notifyDataSetChanged();
    }

    public interface ForecastAdapterOnClickHandler {
        void onClick(Result thisMovie);
    }

    public NewAdapter(ForecastAdapterOnClickHandler clickHandler) {
        mClickHandler = clickHandler;
        mMovies = new ArrayList<>();
    }

    public class ForecastAdapterViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        public ImageView imageView;

        public ForecastAdapterViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.iv_weather_data);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Result movie = mMovies.get(adapterPosition);
            mClickHandler.onClick(movie);
        }
    }

    @Override
    public ForecastAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.list_iteam;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new ForecastAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ForecastAdapterViewHolder holder, int position) {
        String movieImg = mMovies.get(position).getPosterPath();
        Log.d("movieImg",movieImg);
        Picasso.get()
                .load("http://image.tmdb.org/t/p/w185/"+movieImg)
                .error(R.drawable.ic_launcher_background)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }
}
