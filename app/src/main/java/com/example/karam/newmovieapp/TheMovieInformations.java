package com.example.karam.newmovieapp;

/**
 * Created by Karam on 8/26/2018.
 */

public class TheMovieInformations {

    long mvote;
    String mposter;
    String mTitle;
    long mavgVote;
    String mdiscription;
    String mbackdropPath;

    public TheMovieInformations (long vote,String poster ,String title ,long avgVote,String discription, String backdropPath){
        mvote=vote;
        mposter=poster;
        mTitle=title;
        mavgVote=avgVote;
        mdiscription=discription;
        mbackdropPath=backdropPath;
    }


    public   long getMvote(){
        return mvote;
    }
    public String getMposter(){
        return mposter;
    }
    public String getmTitle(){
        return  mTitle;
    }
    public  long getMavgVote(){
        return mavgVote;
    }
    public String getMdiscription(){
        return mdiscription;
    }
    public  String getMbackdropPath(){
        return  mbackdropPath;
    }
}
