package com.example.karam.newmovieapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.net.URL;

public class MainActivity extends AppCompatActivity implements NewAdapter.ForecastAdapterOnClickHandler {
    private static final String url_For_Most_popular =
            "http://api.themoviedb.org/3/movie/popular?api_key=b8d7f72abee18fd93012e158e9211297";
    private static final String url_For_high_rated =
            "http://api.themoviedb.org/3/movie/top_rated?api_key=b8d7f72abee18fd93012e158e9211297";
    private static String general_Url = url_For_Most_popular;
    private RecyclerView mRecyclerView;
    private TextView mErrorMessageDisplay;
    private ProgressBar mLoadingIndicator;
    private NewAdapter newadater;
    public Context context = MainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_forecast);
        mErrorMessageDisplay = (TextView) findViewById(R.id.tv_error_message_display);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(layoutManager);
        //mRecyclerView.setHasFixedSize(true);
        newadater = new NewAdapter(this);
        mRecyclerView.setAdapter(newadater);
        mLoadingIndicator = (ProgressBar) findViewById(R.id.pb_loading_indicator);
        loadWeatherData();
    }

    private void loadWeatherData() {
        showWeatherDataView();
        new FetchMovieTask().execute(general_Url);

    }

    private void showWeatherDataView() {
        mErrorMessageDisplay.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage() {
        mRecyclerView.setVisibility(View.INVISIBLE);
        mErrorMessageDisplay.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(Result thisMovie) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("MovieObj",thisMovie);
        Intent mIntent = new Intent();
        mIntent.putExtras(bundle);
        //thisMovie = mIntent.getExtras().getParcelable("MovieObj");
        Context context = this;
    }

    private class FetchMovieTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingIndicator.setVisibility(View.VISIBLE);
        }

        @Override

        protected String doInBackground(String... strings) {


            if (strings.length == 0) {
                return null;
            }

            String url = strings[0];

            URL moviesUrl = NetworkUtiles.BuildUrl(url);

            String jsonWeatherResponse = null;
            try {
                jsonWeatherResponse = NetworkUtiles.getResponseFromHttpUrl(moviesUrl);
                Log.d("karam", "alzayat");

                //TheMovieInformations[] simpleJsonMovieData = OpenMovieJsonUtils.
                //      getSimpleMovieStringsFromJson(context, jsonWeatherResponse);
                //Log.d("karam","alzayat");
                return jsonWeatherResponse;
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("karam", "alzayat");

                return null;
            }
        }


        @Override
        protected void onPostExecute(String result) {

            mLoadingIndicator.setVisibility(View.INVISIBLE);
            Log.d("karam", result);
            Gson g = new Gson();
            OpenMovieJsonUtils mOpenMovieJsonUtils = g.fromJson(result, OpenMovieJsonUtils.class);
            if (mOpenMovieJsonUtils != null) {
                newadater.setMovieData(mOpenMovieJsonUtils.getResults());
                showWeatherDataView();

            } else {
                showErrorMessage();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.sort_by_most_popular) {
            general_Url = url_For_Most_popular;
            loadWeatherData();
        }
        if (id == R.id.sort_by_Top_Rated) {
            general_Url = url_For_high_rated;
            loadWeatherData();
        }
        if (id == R.id.sort_by_favoriets) {
        }
        return super.onOptionsItemSelected(item);
    }
}